import React from "react";
import { useForm, Controller } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import Select from "react-select";
import makeAnimated from "react-select/animated";

const schema = yup
    .object({
        firstName: yup.string().required("First Name is required!"),
        lastName: yup.string().required("Last Name is required"),
        gender: yup.string().required("Gender is required"),
        email: yup.string().required("Email is required"),
        phone: yup.string().required("Phone is required"),
        password: yup.string().required("Password is required"),
        confirmPassword: yup.string().required("Confirm Password is required"),
        select: yup.array().min(1, "Select is required").required(),
        category: yup
            .object({
                label: yup.string().required("label is required"),
                value: yup.string().required("value is required"),
            })
            .nullable()
            .required("category is required"),
    })
    .required();

const UserRegistration = ({ defaultValues }) => {
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors, isValid },
        control,
    } = useForm({
        resolver: yupResolver(schema),
        defaultValues,
    });

    const categoryOptions = [
        { label: "Steel", value: "steel" },
        { label: "Rod", value: "rod" },
        { label: "Copper", value: "copper" },
        { label: "Copper", value: "copper" },
    ];

    const genderOption = [
        {
            label: (
                <>
                    <img
                        src="https://instagram.fblr2-2.fna.fbcdn.net/v/t51.2885-19/314726668_493701049371257_1454150282043018879_n.jpg?stp=dst-jpg_s150x150&_nc_ht=instagram.fblr2-2.fna.fbcdn.net&_nc_cat=105&_nc_ohc=aLWnUvuJzZgAX85748n&edm=AOQ1c0wBAAAA&ccb=7-5&oh=00_AfDXvlfGtfCH6neB0weZkdTijuWTafssRN-Z9fQKZ9T7BA&oe=637E84D8&_nc_sid=8fd12b"
                        height="30px"
                        width="30px"
                        alt="Profile"
                    />
                    Male
                </>
            ),
            value: "male",
        },
        { label: "Female", value: "female" },
        { label: "Other", value: "other" },
    ];

    const onSubmit = (data) => {
        console.log(data);
    };

    const customTheme = (theme) => {
        return {
            ...theme,
            colors: {
                ...theme.colors,
                primary: "#facc15",
            },
            "&:hover": {
                borderColor: "red",
            },
        };
    };

    return (
        <React.Fragment>
            <h1>Registration Form</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <label htmlFor="firstname">First Name</label>
                    <input type="text" placeholder="Enter First Name" {...register("firstName")} />
                </div>
                <div>
                    <label htmlFor="lastname">Last Name</label>
                    <input type="text" placeholder="Enter Last Name" {...register("lastName")} />
                </div>
                <div>
                    <select {...register("gender")}>
                        <option value="" key="">
                            Select Gender
                        </option>
                        {genderOption.map((option, index) => (
                            <option value={option.value} key={index}>
                                {option.label}
                            </option>
                        ))}
                    </select>
                </div>
                <div>
                    <label htmlFor="email">Email</label>
                    <input type="email" placeholder="Enter your Email" {...register("email")} />
                </div>
                <div>
                    <label htmlFor="phone">Phone</label>
                    <input type="number" placeholder="Enter your number" {...register("phone")} />
                </div>
                <div>
                    <label htmlFor="password">Password</label>
                    <input type="password" placeholder="Enter your password" {...register("password")} />
                </div>
                <div>
                    <label htmlFor="confirmpassword">Confirm Password</label>
                    <input type="password" placeholder="Enter COnfirm password" {...register("confirmPassword")} />
                </div>
                <div>
                    <Controller
                        control={control}
                        name="select"
                        render={({ field: { onChange, value, ref } }) => {
                            return (
                                <Select
                                    inputRef={ref}
                                    value={genderOption.filter((c) => value.includes(c.value))}
                                    onChange={(val) => onChange(val.map((c) => c.value))}
                                    options={genderOption}
                                    isMulti
                                />
                            );
                        }}
                    />
                </div>
                <div>
                    <Controller
                        control={control}
                        name="category"
                        render={({ field }) => {
                            return <Select {...field} isClearable isSearchable={true} options={categoryOptions} isMulti={false} components={makeAnimated()} theme={customTheme} />;
                        }}
                    />
                </div>
                <input type="submit" value="Register" />
            </form>
        </React.Fragment>
    );
};

export default UserRegistration;

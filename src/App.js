import "./App.css";
import UserRegistration from "./Components/UserRegistration";

function App() {
    const defaultValues = {
        firstName: "Sanjeev",
        lastName: "Singh",
        gender: "female",
        email: "s@gmail.com",
        phone: "123123",
        password: "123123",
        confirmPassword: "123123",
        select: ["male"],
        category: { label: "Male", value: "male" },
    };
    return (
        <div>
            <UserRegistration defaultValues={defaultValues} />
        </div>
    );
}

export default App;
